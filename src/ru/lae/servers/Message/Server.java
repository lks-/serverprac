package ru.lae.servers.Message;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;

/**
 * Создаёт сервер и ждёт подключения клиента.
 * Принимает сообщения от пользователя, обрабатывает их и отпарвляет клиенту
 *
 * @author Lukaschevich A.
 */
public class Server {
    public static void main(String[] args) throws IOException {
        String message;
        BufferedReader in;
        BufferedWriter out;
        Boolean start = true;
        ServerSocket serverSocket = new ServerSocket(8080);
        System.out.println("Ждём подключение...");

        try (Socket clientSocket = serverSocket.accept()) {
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream(), Charset.forName("UTF-8")));
            out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream(), Charset.forName("UTF-8")));
            System.out.println("Присоеденился пользователь " + clientSocket.getInetAddress().toString());

            while (start) {
                message = in.readLine();
                if (message.equals("СТОП")) {
                    start = false;
                } else {
                    System.out.println("Поспупило сообщение - " + message);
                    out.write(message + "\r\n");
                    out.flush();
                    System.out.println("Отправленно " + message);
                }
            }
            serverSocket.close();
        }
    }
}
