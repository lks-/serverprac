package ru.lae.servers.Chat;

import java.io.IOException;

public interface ConnectionListener {

    void onConnectionReady(Connection tcpConnection);

    void onReceiveString(Connection tcpConnection, String s);

    void onDisconnect(Connection tcpConnection);

    void onException(Connection tcpConnection, IOException e);

}
