package ru.lae.servers.Message;

import java.io.*;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.Scanner;

/**
 * Подключается к серверу.
 * Отправляет собщения серверу и примает сообщения от сервера
 *
 * @author Lukaschevich A.
 */
public class Client {
    public static void main(String[] args) throws IOException {
        Scanner mes = new Scanner(System.in);
        String message;
        Boolean start = true;
        BufferedReader in;
        BufferedWriter out;
        try (Socket socket = new Socket("localhost", 8080)) {
            in = new BufferedReader(new InputStreamReader(socket.getInputStream(), Charset.forName("UTF-8")));
            out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), Charset.forName("UTF-8")));
            while (start) {
                message = mes.nextLine();
                if (message.equals("СТОП")) {
                    out.write("СТОП");
                    out.flush();
                    start = false;
                } else {
                    out.write(message + "\r\n");
                    out.flush();
                    System.out.println("Сообщение серверу " + message);
                    System.out.println("__________________________________________________________________");
                    System.out.println("Прислал сервер " + in.readLine());
                }
            }
        }
    }
}

